 ﻿<#
 Template to run a command (Line 17) on diffrent computers.
 It is much faster to run multiple jobs on remote computers than running all those jobs on your worksatation.
 Please make sure to not create to many jobs at once. Lets say if you ping 1000 hosts it would be better to 
 ping multiple hosts in one job, because the amount of jobs (every job creates a process) would slow your machine down.
 1. Execute Command
 2. Wait for all Commands to finish
 3. Receive successfull commands
 4. Receive failed commands
 #>
 
 $targets = Get-ADComputer -Filter {Name -like "computername"} 
 
 Remove-Job -Force -Name * #not necessary, just for testing.
 
 foreach($target in $targets){
    Invoke-Command -ComputerName $target.Name -ScriptBlock { "your command here" } -AsJob -JobName $target
 }
 
 write-host "Jobs created."
 
 foreach($target in $targets){
     Wait-Job -Name $target.Name
 }
 
 $jobs = Get-Job | where {$_.State -eq "Completed"}
 
 $failedjobs = Get-Job | where {$_.State -ne "Completed"}
 
 foreach ($failedjob in $failedjobs){
    $failedjob.Name | Add-Content $PSScriptRoot\failedjobs.txt
    $result = Receive-Job -Name $failedjob.Name
    $result | Add-Content $PSScriptRoot\failedjobs.txt
 }
 
 foreach($job in $jobs){
     $result = Receive-Job -Name $job.Name
     $result | Add-Content $PSScriptRoot\result.txt
 }