function ptrg_commands()
{
    Param
    (
        [CmdletBinding()]
        [OutputType([string])]
        [parameter(Mandatory=$true)][ValidateSet("pause","scannow","start","getxml","error_on","error_off")][String]$command,
        [parameter()][String]$sensor_id,
        [parameter(Mandatory=$true)][String]$prtgserver,
        [parameter(Mandatory=$true)][String]$username,
        [parameter(Mandatory=$true)][String]$password
    )
    #If no password is defined, aks the user for a password 
    if ( $password -eq $nul -or $password -eq ""){
        $password_secure = Read-Host -AsSecureString Type Password
        $credentials = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($password_secure)
        $password=[System.Runtime.InteropServices.Marshal]::PtrToStringAuto($credentials)
    }
    #Prtg rest api
    switch ([string]$command){
        "pause"{
            (New-Object net.webclient).DownloadString("http://"+$prtgserver+"/api/pause.htm?id=$sensor_id&action=0&username=$username&password=$password") | Out-Null
        }
        "scannow"{
            (New-Object net.webclient).DownloadString("http://"+$prtgserver+"/api/scannow.htm?id=$sensor_id&username=$username&password=$password") | Out-Null
        }
        "start"{
            (New-Object net.webclient).DownloadString("http://"+$prtgserver+"/api/pause.htm?id=$sensor_id&action=1&username=$username&password=$password") | Out-Null
        }
        "getxml"{
            (New-Object net.webclient).DownloadString("http://"+$prtgserver+"/api/getstatus.xml?username=$username&password=$password") | Out-File -FilePath $PSScriptRoot\Logs\xml.txt
        }
        "error_on"{
            (New-Object net.webclient).DownloadString("http://"+$prtgserver+"/api/simulate.htm?id=$sensor_id&action=1&username=$username&password=$password") | Out-Null
        }
        "error_off"{
            (New-Object net.webclient).DownloadString("http://"+$prtgserver+"/api/simulate.htm?id=$sensor_id&action=0&username=$username&password=$password") | Out-Null
        }
        default{ log_write -string "command not found"}
    }
    [string]$out_debug = "Command "+ $command+" was sent to Sensor ID: "+ $sensor_id
    Write-Debug -string $out_debug
}