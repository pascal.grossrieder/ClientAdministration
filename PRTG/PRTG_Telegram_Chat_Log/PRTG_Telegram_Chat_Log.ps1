<#
PRTG Sensor checking a Telegram Group Chat (No Supergroups).

Steps:
 1. Activate Telegram
 2. Send a message to @BotFather
 3. Send /newbot
 4. Name your bot
 5. Fill in the token of your bot.
 6. /setprivacy in order to make your bot able to receive messages of group chats.
 7. Add your bot to the chat.
#>

$prtg_folder = "C:\Program Files (x86)\PRTG Network Monitor"
Set-Location "C:\Scripts\infralog_bot" #PATH #Set location of the Script 

function write-html($prtg_folder)
{
    #Creates a simple html to use it in PRTG Maps
    $html_1 = "
    <!DOCTYPE html>
    <html>
        <head>
		    <link rel=`"stylesheet`" type=`"text/css`" href=`"style.css`">
            <meta http-equiv=`"refresh`" content=`"5`">
        </head>
        <body>
    "
    $messages = get-content (".\"+(get-date -format ddMMyy)+"_chatlog.txt")| select -last 3 #Reading todays log 
    [array]::Reverse($messages) #reverse the logs, to get the newest entry first
    $counter = 1
    foreach($message in $messages){
        $message = $message.split(";")
        $text = $message[3]
        $time = $message[0]
        $firstname = $message[2]
        $user = $message[1]
        $headers+= "<div id=`"div-"+$counter+"`"><h2>"+$time+" "+$user+" "+$firstname+"</h2><h1>"+$text+"</h1></div>"
        $counter ++
    }
    $html_2 = "
        </body>
    </html>
    "
    #Add the map in order to add it to the PRTG Network Maps
    $htmlpath = $prtg_folder+"\webroot\log_map.htm" #PATH
    Remove-Item -Path $htmlpath -Force -ErrorAction SilentlyContinue
    Add-Content -Value $html_1 -Path $htmlpath
    Add-Content -Value $headers -Path $htmlpath
    Add-Content -Value $html_2 -Path $htmlpath
}

function prtg_popup($id, $title, $message, $prtg_folder){
    #Insert Text in to the scripts_custom.js (predefined by Paessler PRTG)
    #https://kb.paessler.com/en/topic/51293-how-can-i-display-a-message-to-all-prtg-users-using-the-web-gui
    $output = "`$(document).ready(function()`r`n"
    $output += "{`r`n"
    $output += "	    _Prtg.Growls.add({`r`n"
    $output += "	    id: '"+$id+"',`r`n"
    $output += "	    type: `"error`",`r`n"
    $output += "	    time: _Prtg.Options.refreshInterval * 3,`r`n"
    $output += "	    title: '"+$title+"',`r`n"
    $output += "	    message: '"+$message+ "'`r`n"
    $output += "	  });`r`n"
    $output += "  }`r`n"
    $output += ");"
    $output | Set-Content ($prtg_folder+"\webroot\javascript\scripts_custom.js") #PATH #Hier holt PRTG den Javascript Teil an und bindet das Script in der Website ein.
}
<# Example Output "C:\Program Files (x86)\PRTG Network Monitor\webroot\javascript\scripts_custom.js"
$(document).ready(function()
{
	    _Prtg.Growls.add({
	    id: '123',
	    type: "error",
	    time: _Prtg.Options.refreshInterval * 3,
	    title: 'titel',
	    message: 'test'
	  });
  }
);
#>


#This line is needed, because PRTG needs to change its sensor state back to OK, to receive the next message.
if(Test-Path .\statuserror.txt){
    Remove-Item .\statuserror.txt
    #It is necessary to remove the scripts_custom.js file to remove the message on the prtg system.
    Set-Content ($prtg_folder+"\webroot\javascript\scripts_custom.js") -Value $null
    write-host "0:OK"
    exit 0
}

#Telegram start
$token = "YOUR TOKEN HERE" #Insert token here.
$uri = "https://api.telegram.org/bot"+$token+"/"
$getupdates = $uri+"getUpdates"
$offset = Get-Content -Path .\offset.txt -ErrorAction SilentlyContinue #The offset is needed to check which messages were received before.
#Telegram end
[datetime]$UnixEpoch = '1970-01-01 00:00:00' #To convert the Telegram date into the Get-Date Format.

#Correct the daylight saving time.
if((get-date).IsDaylightSavingTime()){
    $timezone = 2
}else{
    $timezone = 1
}

$json = Invoke-WebRequest -Uri $getupdates -UseBasicParsing -Body @{offset=$offset} | ConvertFrom-Json #Proxy
$offset = $json.result[0].update_id

if ($offset -eq $null -or $offset -eq ""){}else{
    $offset + 1 | Set-Content -Path .\offset.txt -Force
}

#The bot will only receive group messages and no empty messages (like "xyz was added")
if($json.result[0].message.chat.type -like "group" -and $json.result[0].message.text.length -gt 0){     
    #File wird benötigt damit PRTG auf den Status OK zurück Fällt
    New-Item -Path .\statuserror.txt -ItemType File | out-null #Creates a File in order to let PRTG fall back to the OK Status	

    $date = (get-date $UnixEpoch.AddSeconds($json.result[0].message.date + $timezone * 3600) -Format ddMMyyHHmm)
    $username = $json.result[0].message.from.username
    $text = $json.result[0].message.text
    $firstname = $json.result[0].message.from.first_name
    $message = $date+";"+$username+";"+$firstname+";"+$text
    $message -replace "`n|`r" , "" | add-content (".\"+(get-date -format ddMMyy)+"_chatlog.txt")
    "1:"+$message
    
    prtg_popup -id (Get-Random -Minimum 1 -Maximum 999) -title ("Message from"+ $firstname+" "+ $username) -message $text -prtg_folder $prtg_folder
    write-html -prtg_folder $prtg_folder
    exit 1  
}

if($error.count -gt 0){
    "Logs "+(get-date) | Add-Content .\error.txt
    $Error | Add-Content .\error.txt
}
write-host "0:OK"
exit 0
