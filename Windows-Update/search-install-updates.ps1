$Criteria = "IsInstalled=0 and Type='Software' and BrowseOnly=0"
$Searcher = New-Object -ComObject Microsoft.Update.Searcher
$Searcher.Online = $True
$SearchResult = $Searcher.Search($Criteria).Updates
sleep(10)
$Session = New-Object -ComObject Microsoft.Update.Session
$Downloader |Get-Member
$Downloader = $Session.CreateUpdateDownloader()
$Downloader.Updates = $SearchResult
$Downloader.Download()
sleep(10)
$Installer = New-Object -ComObject Microsoft.Update.Installer
$Installer.Updates = $SearchResult
$Result = $Installer.Install()
if ($Result.RebootRequire -eq $False){
    #Insert Reboot
    write-host "Reboot Required"
}