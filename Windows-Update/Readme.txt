Updated 03.05.2016 by GRP

Usage:
Updates multiple Clients or Servers without WSUS.

The Exes are made by https://ps2exe.codeplex.com/
search-install-updates.exe search updates, download updates and install updates. (no reboot!)
search-download-updates.exe search updates and download updates.
If you need to reboot your servers automatically, you need to replace a line in the search-install-updates.ps1 and "compile/wrap" it with ps2exe.
PsExec.exe is needed, you need to download it yourself (copyright issues).


1.  Copy psexec.exe in to the folder (Sysinternals Suite by Microsoft)

2.  Copy the Folder to an accessible share.

3.  Run the Command 

4.  PsExec.exe \\remotecomputer -f -c -s -d \\computername\windows-update\search-install-updates.exe
    PsExec.exe \\remotecomputer -f -c -s -d \\computername\windows-update\search-download-updates.exe

5.  PsExec.exe will copy the scripts of the share and will send it to the remotesystem. 

6.  To check if it works, look in to the CBS.log